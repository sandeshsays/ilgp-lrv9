# Tech-Admin | Laravel 9 + Bootstrap 4

Tech-Admin is Admin Panel With Preset of Roles, Permissions, ACL, User Management, Profile Management.




## Features

- Mobile Responsive Bootstrap 4 Design
- User Management with Roles
- Role Management
- Permissions Management
- Access Control List (ACL)
- Laravel 9 + Bootstrap 4


## Tech Stack

**Client:** HTML, CSS, JavaScript, jQuery, VueJs, Bootstrap 4

**Server:** PHP, Laravel 9

**DataBase:** MySql, PgSql


